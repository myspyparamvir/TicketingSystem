CREATE DATABASE  IF NOT EXISTS `paramvirticketingsystem` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `paramvirticketingsystem`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: paramvirticketingsystem
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.32-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departments` (
  `department_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `department_code` varchar(255) DEFAULT NULL,
  `department_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`department_id`),
  UNIQUE KEY `UK_89g8qie2y696a3tarmty43sq9` (`department_code`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departments`
--

LOCK TABLES `departments` WRITE;
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;
INSERT INTO `departments` VALUES (1,'OMS','Ordering Management System'),(2,'CRM','Customer Relationship Management'),(3,'BIL','Billing System');
/*!40000 ALTER TABLE `departments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employees` (
  `employee_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `department_id` bigint(20) NOT NULL,
  `designation` varchar(255) DEFAULT NULL,
  `manager_employee_id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`employee_id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employees`
--

LOCK TABLES `employees` WRITE;
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
INSERT INTO `employees` VALUES (1,1,'SoftwareDeveloper',2,'Ankit Shriwastav'),(3,1,'SoftwareDeveloper',2,'Priya'),(9,1,'SoftwareDeveloper',2,'Gunjan Sharma'),(10,1,'SoftwareDeveloper',2,'Sunita Verma'),(2,1,'Manager',2,'Manav Gupta'),(4,2,'SoftwareDeveloper',6,'Varun Gupta'),(5,2,'SoftwareDeveloper',6,'Sunita Chauhan'),(6,2,'Manager',6,'Abhishek Chowdhary'),(7,2,'SoftwareDeveloper',6,'Varun Gupta'),(8,2,'SoftwareDeveloper',6,'Varun Gupta'),(11,3,'SoftwareDeveloper',16,'Sanjeev '),(12,3,'SoftwareDeveloper',16,'Devendra'),(13,3,'SoftwareDeveloper',16,'Piyush'),(14,3,'SoftwareDeveloper',16,'Priyanka'),(15,3,'SoftwareDeveloper',16,'Puja');
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (1);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tickets`
--

DROP TABLE IF EXISTS `tickets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tickets` (
  `ticket_number` bigint(20) NOT NULL AUTO_INCREMENT,
  `assign_time` datetime DEFAULT NULL,
  `assigned_to_department` bigint(20) DEFAULT NULL,
  `closed_by` bigint(20) DEFAULT NULL,
  `closing_time` datetime DEFAULT NULL,
  `created_by` bigint(20) NOT NULL,
  `creation_time` datetime NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `assigned_to_employee_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ticket_number`),
  KEY `FK8mqm1x5wjinqh0b4j4trodejf` (`assigned_to_employee_id`)
) ENGINE=MyISAM AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tickets`
--

LOCK TABLES `tickets` WRITE;
/*!40000 ALTER TABLE `tickets` DISABLE KEYS */;
INSERT INTO `tickets` VALUES (1,'2018-05-26 10:46:31',1,NULL,NULL,1,'2018-05-26 10:40:47','d','Open',1),(2,'2018-05-26 10:53:28',3,NULL,NULL,1,'2018-05-26 10:45:07','d','Open',14),(3,'2018-05-26 10:46:40',1,NULL,NULL,1,'2018-05-26 10:45:08','d','Open',2),(4,'2018-05-26 10:46:43',2,NULL,NULL,1,'2018-05-26 10:45:08','d','Open',5),(5,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:08','d','Open',NULL),(6,'2018-05-26 10:46:47',2,NULL,NULL,1,'2018-05-26 10:45:08','d','Open',7),(7,'2018-05-26 10:46:50',2,NULL,NULL,1,'2018-05-26 10:45:08','d','Open',7),(8,'2018-05-26 10:46:51',2,NULL,NULL,1,'2018-05-26 10:45:09','d','Open',7),(9,'2018-05-26 10:46:52',2,NULL,NULL,1,'2018-05-26 10:45:09','d','Open',7),(10,'2018-05-26 10:52:34',2,NULL,NULL,1,'2018-05-26 10:45:09','d','Open',8),(11,'2018-05-26 10:52:36',2,NULL,NULL,1,'2018-05-26 10:45:09','d','Open',8),(12,'2018-05-26 10:52:37',2,NULL,NULL,1,'2018-05-26 10:45:09','d','Open',8),(13,'2018-05-26 10:52:38',2,NULL,NULL,1,'2018-05-26 10:45:10','d','Open',8),(14,'2018-05-26 10:52:43',1,NULL,NULL,1,'2018-05-26 10:45:10','d','Open',9),(15,'2018-05-26 10:52:44',1,NULL,NULL,1,'2018-05-26 10:45:10','d','Open',9),(16,'2018-05-26 10:52:45',1,NULL,NULL,1,'2018-05-26 10:45:10','d','Open',9),(17,'2018-05-26 10:52:46',1,NULL,NULL,1,'2018-05-26 10:45:11','d','Open',9),(18,'2018-05-27 15:24:57',1,NULL,NULL,1,'2018-05-26 10:45:11','d','Open',1),(19,'2018-05-26 10:53:01',3,NULL,NULL,1,'2018-05-26 10:45:11','d','Open',11),(20,'2018-05-26 10:53:03',3,NULL,NULL,1,'2018-05-26 10:45:11','d','Open',11),(21,'2018-05-26 10:53:04',3,NULL,NULL,1,'2018-05-26 10:45:11','d','Open',11),(22,'2018-05-26 10:53:07',3,NULL,NULL,1,'2018-05-26 10:45:12','d','Open',11),(23,'2018-05-26 10:53:09',3,NULL,NULL,1,'2018-05-26 10:45:12','d','Open',11),(24,'2018-05-26 10:53:10',3,NULL,NULL,1,'2018-05-26 10:45:12','d','Open',11),(25,'2018-05-26 10:53:11',3,NULL,NULL,1,'2018-05-26 10:45:12','d','Open',11),(26,'2018-05-26 10:53:15',3,NULL,NULL,1,'2018-05-26 10:45:12','d','Open',12),(27,'2018-05-26 10:53:21',3,NULL,NULL,1,'2018-05-26 10:45:13','d','Open',13),(28,'2018-05-26 10:54:17',3,NULL,NULL,1,'2018-05-26 10:45:13','d','Open',15),(29,'2018-05-26 10:54:21',3,NULL,NULL,1,'2018-05-26 10:45:13','d','Open',15),(30,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:13','d','Open',NULL),(31,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:14','d','Open',NULL),(32,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:14','d','Open',NULL),(33,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:15','d','Open',NULL),(34,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:16','d','Open',NULL),(35,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:16','d','Open',NULL),(36,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:16','d','Open',NULL),(37,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:16','d','Open',NULL),(38,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:16','d','Open',NULL),(39,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:17','d','Open',NULL),(40,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:17','d','Open',NULL),(41,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:17','d','Open',NULL),(42,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:17','d','Open',NULL),(43,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:17','d','Open',NULL),(44,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:18','d','Open',NULL),(45,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:18','d','Open',NULL),(46,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:18','d','Open',NULL),(47,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:18','d','Open',NULL),(48,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:18','d','Open',NULL),(49,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:19','d','Open',NULL),(50,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:19','d','Open',NULL),(51,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:19','d','Open',NULL),(52,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:19','d','Open',NULL),(53,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:19','d','Open',NULL),(54,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:20','d','Open',NULL),(55,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:20','d','Open',NULL),(56,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:20','d','Open',NULL),(57,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:20','d','Open',NULL),(58,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:20','d','Open',NULL),(59,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:20','d','Open',NULL),(60,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:21','d','Open',NULL),(61,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:21','d','Open',NULL),(62,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:21','d','Open',NULL),(63,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:21','d','Open',NULL),(64,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:21','d','Open',NULL),(65,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:22','d','Open',NULL),(66,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:22','d','Open',NULL),(67,NULL,NULL,NULL,NULL,1,'2018-05-26 10:45:22','d','Open',NULL);
/*!40000 ALTER TABLE `tickets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-27 18:03:15
