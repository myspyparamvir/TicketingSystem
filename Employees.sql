/*
-- Query: select * from employees
LIMIT 0, 1000

-- Date: 2018-05-26 10:59
*/
INSERT INTO `employees` (`employee_id`,`department_id`,`designation`,`manager_employee_id`,`name`) VALUES (1,1,'SoftwareDeveloper',2,'Ankit Shriwastav');
INSERT INTO `employees` (`employee_id`,`department_id`,`designation`,`manager_employee_id`,`name`) VALUES (3,1,'SoftwareDeveloper',2,'Priya');
INSERT INTO `employees` (`employee_id`,`department_id`,`designation`,`manager_employee_id`,`name`) VALUES (9,1,'SoftwareDeveloper',2,'Gunjan Sharma');
INSERT INTO `employees` (`employee_id`,`department_id`,`designation`,`manager_employee_id`,`name`) VALUES (10,1,'SoftwareDeveloper',2,'Sunita Verma');
INSERT INTO `employees` (`employee_id`,`department_id`,`designation`,`manager_employee_id`,`name`) VALUES (2,1,'Manager',2,'Manav Gupta');
INSERT INTO `employees` (`employee_id`,`department_id`,`designation`,`manager_employee_id`,`name`) VALUES (4,2,'SoftwareDeveloper',6,'Varun Gupta');
INSERT INTO `employees` (`employee_id`,`department_id`,`designation`,`manager_employee_id`,`name`) VALUES (5,2,'SoftwareDeveloper',6,'Sunita Chauhan');
INSERT INTO `employees` (`employee_id`,`department_id`,`designation`,`manager_employee_id`,`name`) VALUES (6,2,'Manager',6,'Abhishek Chowdhary');
INSERT INTO `employees` (`employee_id`,`department_id`,`designation`,`manager_employee_id`,`name`) VALUES (7,2,'SoftwareDeveloper',6,'Varun Gupta');
INSERT INTO `employees` (`employee_id`,`department_id`,`designation`,`manager_employee_id`,`name`) VALUES (8,2,'SoftwareDeveloper',6,'Varun Gupta');
INSERT INTO `employees` (`employee_id`,`department_id`,`designation`,`manager_employee_id`,`name`) VALUES (11,3,'SoftwareDeveloper',16,'Sanjeev ');
INSERT INTO `employees` (`employee_id`,`department_id`,`designation`,`manager_employee_id`,`name`) VALUES (12,3,'SoftwareDeveloper',16,'Devendra');
INSERT INTO `employees` (`employee_id`,`department_id`,`designation`,`manager_employee_id`,`name`) VALUES (13,3,'SoftwareDeveloper',16,'Piyush');
INSERT INTO `employees` (`employee_id`,`department_id`,`designation`,`manager_employee_id`,`name`) VALUES (14,3,'SoftwareDeveloper',16,'Priyanka');
INSERT INTO `employees` (`employee_id`,`department_id`,`designation`,`manager_employee_id`,`name`) VALUES (15,3,'SoftwareDeveloper',16,'Puja');
