package com.karwal.paramvir.ticketingsystem.service;

import com.karwal.paramvir.ticketingsystem.entity.Department;
import com.karwal.paramvir.ticketingsystem.entity.Employee;
import com.karwal.paramvir.ticketingsystem.entity.Ticket;
import com.karwal.paramvir.ticketingsystem.exception.TicketingSystemException;

import java.util.List;

public interface TicketingService {

    boolean createNewTicket(Ticket ticket)throws TicketingSystemException;
    Ticket retrieveTicket(Long ticketNumber)throws TicketingSystemException;
    boolean assignTicket(Long ticketNumber, Long assigneeId)throws TicketingSystemException;
    boolean closeTicket(Long ticketNumber,Long closedBy)throws TicketingSystemException;
    List<Ticket> findPendingTicketsForDepartment(Long departmentId)throws TicketingSystemException;
    List<Ticket> retrieveAllTickets()throws TicketingSystemException;
    List<Employee> retrieveAllEmployees()throws TicketingSystemException;
    List<Employee> retrieveEmployeesUnderManger(Long managerId)throws TicketingSystemException;
    List<Ticket> retrievePendingTicketsForManager(Long managerId)throws TicketingSystemException;
    List<Department> searchDepartmentsByName(String departmentName)throws TicketingSystemException;
}
