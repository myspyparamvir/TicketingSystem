package com.karwal.paramvir.ticketingsystem.service.impl;

import com.karwal.paramvir.ticketingsystem.entity.Department;
import com.karwal.paramvir.ticketingsystem.entity.Employee;
import com.karwal.paramvir.ticketingsystem.entity.Ticket;
import com.karwal.paramvir.ticketingsystem.exception.TicketingSystemException;
import com.karwal.paramvir.ticketingsystem.repository.DepartmentRepository;
import com.karwal.paramvir.ticketingsystem.repository.EmployeeRepository;
import com.karwal.paramvir.ticketingsystem.repository.TicketRepository;
import com.karwal.paramvir.ticketingsystem.service.TicketingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class TicketingServiceImpl implements TicketingService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TicketingServiceImpl.class);
    private String TICKET_STATUS_OPEN="Open";
    private String TICKET_STATUS_CLOSED="Closed";


    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private TicketRepository ticketRepository;

    public boolean createNewTicket(Ticket ticket) throws TicketingSystemException {
        try {
            ticket.setStatus(TICKET_STATUS_OPEN);
            ticketRepository.save(ticket);
            return true;
        }
        catch(Exception e) {
            String errorMessage=e.getMessage();
            LOGGER.error(errorMessage);
            throw new TicketingSystemException(errorMessage);
        }
    }

    public Ticket retrieveTicket(Long ticketNumber) throws TicketingSystemException
    {

        Optional<Ticket> optionalTicket= ticketRepository.findById(ticketNumber);
        if(optionalTicket.isPresent())
        {
            return optionalTicket.get();
        }
        else{
            String errorMessage="No ticket exists with ticket number: "+ticketNumber;
            LOGGER.error(errorMessage);
            throw new TicketingSystemException(errorMessage);
        }
    }

    public boolean assignTicket(Long ticketNumber, Long assigneeId)throws TicketingSystemException
    {
        Ticket ticket=retrieveTicket(ticketNumber);

        Optional<Employee> employeeOptional=employeeRepository.findById(assigneeId);
        if(employeeOptional.isPresent() )
        {
            Employee employee=employeeOptional.get();
            ticket.setAssignedToDepartment(employee.getDepartment());
            ticket.setAssignedTo(employee);
            ticket.setAssignTime(new Date());
            ticketRepository.save(ticket);

        }
        else
        {
            String errorMessage="No employee exists with employee id: "+assigneeId;
            LOGGER.error(errorMessage);
            throw new TicketingSystemException(errorMessage);
        }

        return true;
    }

    @Override
    public boolean closeTicket(Long ticketNumber, Long closedBy) throws TicketingSystemException {
        Ticket ticket=retrieveTicket(ticketNumber);

        Optional<Employee> employeeOptional=employeeRepository.findById(closedBy);
        if(employeeOptional.isPresent() )
        {
            ticket.setClosedBy(closedBy);
            ticket.setClosingTime(new Date());
            ticket.setStatus(TICKET_STATUS_CLOSED);
            ticketRepository.save(ticket);
        }
        else
        {
            String errorMessage="No employee exists with employee id: "+closedBy;
            LOGGER.error(errorMessage);
            throw new TicketingSystemException(errorMessage);
        }
        return true;
    }

    @Override
    public List<Ticket> findPendingTicketsForDepartment(Long departmentId) throws TicketingSystemException {
        return ticketRepository.findByAssignedToDepartmentAndStatus(departmentId,TICKET_STATUS_OPEN);
    }


    @Override
    public List<Ticket> retrieveAllTickets() throws TicketingSystemException {
        return ticketRepository.findAll();
    }

    @Override
    public List<Employee> retrieveAllEmployees() throws TicketingSystemException {
        return employeeRepository.findAll();
    }

    @Override
    public List<Employee> retrieveEmployeesUnderManger(Long managerId) throws TicketingSystemException {
        return employeeRepository.findByManagerEmployeeId(managerId);
    }

    @Override
    public List<Ticket> retrievePendingTicketsForManager(Long managerId) throws TicketingSystemException {

        List<Employee> employeesUnderManager=retrieveEmployeesUnderManger(managerId);

        if(employeesUnderManager!=null && !employeesUnderManager.isEmpty())
        {
            List<Ticket> pendingTickets=ticketRepository.findByAssignedToInAndStatus(employeesUnderManager,TICKET_STATUS_OPEN);
            return pendingTickets;
        }

        return new ArrayList<>();

    }

    @Override
    public List<Department> searchDepartmentsByName(String departmentName) throws TicketingSystemException {
        return departmentRepository.findByDepartmentNameContainingIgnoreCase(departmentName);
    }
}
