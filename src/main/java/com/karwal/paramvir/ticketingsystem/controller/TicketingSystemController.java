package com.karwal.paramvir.ticketingsystem.controller;

import com.karwal.paramvir.ticketingsystem.entity.Department;
import com.karwal.paramvir.ticketingsystem.entity.Employee;
import com.karwal.paramvir.ticketingsystem.entity.Ticket;

import com.karwal.paramvir.ticketingsystem.exception.TicketingSystemException;
import com.karwal.paramvir.ticketingsystem.service.TicketingService;
import com.karwal.paramvir.ticketingsystem.service.impl.TicketingServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

//@CrossOrigin(origins = "http://localhost:4200")
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/request")
public class TicketingSystemController {

    @Autowired
    private TicketingService ticketingService;

    @GetMapping("/retrieveEmployees")
    public List<Employee> retrieveAllEmployees() throws TicketingSystemException {
    return ticketingService.retrieveAllEmployees();
    }


    @PostMapping("/createTicket")
    public boolean createTicket(@RequestBody Ticket ticket) throws TicketingSystemException {
        boolean ticketCreationStatus=ticketingService.createNewTicket(ticket);
        return ticketCreationStatus;
    }

    @GetMapping("/retrieveTicket/{ticketnumber}")
    public Ticket retrieveTicket(@PathVariable("ticketnumber") Long ticketNumber) throws TicketingSystemException {
        Ticket ticket=ticketingService.retrieveTicket(ticketNumber);
        return ticket;
    }

    @GetMapping("/retrieveAllTickets")
    public List<Ticket> retrieveAllTickets() throws TicketingSystemException {
        List<Ticket> ticketList=ticketingService.retrieveAllTickets();
        return ticketList;
    }

    @PutMapping("/assignTicket/{ticketnumber}/{assigneeid}")
    public boolean assignTicket(@PathVariable("ticketnumber") Long ticketNumber,@PathVariable("assigneeid")Long assigneeId) throws TicketingSystemException {
        boolean ticketAssignStatus=ticketingService.assignTicket(ticketNumber,assigneeId);
        return ticketAssignStatus;
    }

    @PutMapping("/closeTicket/{ticketNumber}/{closedby}")
    public boolean closeTicket(@PathVariable("ticketNumber") Long ticketNumber,@PathVariable("closedby") Long closedBy) throws TicketingSystemException {
        boolean ticketClosingStatus=ticketingService.closeTicket(ticketNumber,closedBy);
        return ticketClosingStatus;
    }

    @GetMapping("/retrievePendingDepartmentTickets/{departmentId}")
    public List<Ticket> retrievePendingDepartmentTickets(@PathVariable("departmentId")Long departmentId) throws TicketingSystemException {
        List<Ticket> pendingTickets=ticketingService.findPendingTicketsForDepartment(departmentId);
        return pendingTickets;
    }


    @GetMapping("/retrievePendingTicketsForManager/{managerId}")
    public List<Ticket> retrievePendingTicketsForManager(@PathVariable("managerId")Long mangerId) throws TicketingSystemException {
        List<Ticket> pendingTicketsUnderManager=ticketingService.retrievePendingTicketsForManager(mangerId);
        return pendingTicketsUnderManager;
    }

    @CrossOrigin
//    @GetMapping("/searchDepartmentsByName/{departmentName}")
    @RequestMapping(value = {"/searchDepartmentsByName", "/searchDepartmentsByName/{departmentName}"}, method = RequestMethod.GET)
    public List<Department> searchDepartmentsByName(@PathVariable Optional<String> departmentName) throws TicketingSystemException{
        if(!departmentName.isPresent() || departmentName.get().isEmpty())
        {
            return new ArrayList<>();
        }
        else {
            List<Department> matchingDepartments = ticketingService.searchDepartmentsByName(departmentName.get());
            return matchingDepartments;
        }
    }


}
