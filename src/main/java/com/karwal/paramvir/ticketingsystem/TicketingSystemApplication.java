package com.karwal.paramvir.ticketingsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.EntityListeners;

@EntityListeners(AuditingEntityListener.class)
@SpringBootApplication
public class TicketingSystemApplication {

	public static void main(String[] args) {

		SpringApplication.run(TicketingSystemApplication.class, args);
	}
}
