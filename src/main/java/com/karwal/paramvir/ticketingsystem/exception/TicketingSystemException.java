package com.karwal.paramvir.ticketingsystem.exception;

public class TicketingSystemException extends Exception{
    String errorMessage;
    public TicketingSystemException(String errorMessage) {
        this.errorMessage=errorMessage;

    }

    @Override
    public String toString() {
        return errorMessage;
    }

    @Override
    public String getMessage() {
        return errorMessage;
    }
}
