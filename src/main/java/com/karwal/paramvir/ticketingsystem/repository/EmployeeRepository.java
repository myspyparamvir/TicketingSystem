package com.karwal.paramvir.ticketingsystem.repository;

import com.karwal.paramvir.ticketingsystem.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee,Long>{
    List<Employee> findByManagerEmployeeId(Long managerId);
}
