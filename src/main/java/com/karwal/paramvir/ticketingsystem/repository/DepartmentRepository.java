package com.karwal.paramvir.ticketingsystem.repository;

import com.karwal.paramvir.ticketingsystem.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DepartmentRepository extends JpaRepository<Department,Long>{

    List<Department> findByDepartmentNameContainingIgnoreCase(String departmentName);

}
