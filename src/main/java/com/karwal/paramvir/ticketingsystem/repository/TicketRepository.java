package com.karwal.paramvir.ticketingsystem.repository;

import com.karwal.paramvir.ticketingsystem.entity.Employee;
import com.karwal.paramvir.ticketingsystem.entity.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TicketRepository extends JpaRepository<Ticket,Long> {

    List<Ticket> findByAssignedToDepartmentAndStatus(Long departmentId, String ticketStatus);
    List<Ticket> findByAssignedToInAndStatus(List<Employee> employeeIds, String status);


//    @Query(value= "select ticket_number,status,name as assigned_to,assign_time,creation_time,creation_time,description,department_code,department_name from (select t.*,d.* from tickets t join departments d on t.assigned_to_department=d.department_id where department_id=?0) td join employees e on e.employee_id=td.assigned_to")
}
