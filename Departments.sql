/*
-- Query: select * from departments
LIMIT 0, 1000

-- Date: 2018-05-26 10:58
*/
INSERT INTO `departments` (`department_id`,`department_code`,`department_name`) VALUES (1,'OMS','Ordering Management System');
INSERT INTO `departments` (`department_id`,`department_code`,`department_name`) VALUES (2,'CRM','Customer Relationship Management');
INSERT INTO `departments` (`department_id`,`department_code`,`department_name`) VALUES (3,'BIL','Billing System');
